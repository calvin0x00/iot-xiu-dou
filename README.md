# IoT Xiu-Dou (秀斗) 
Microservice modules demo by Node.js and gRPC for ["IoT-Binding-Bot"](https://docs.google.com/presentation/d/1BjkP5iU0KG8e_DfL0rHCUF8jlgEP8YML3NuynYGRtKw/edit?usp=sharing) - An IoT end-device and user binding at LINE bot demo/side project.

## Get start
```
> git clone https://gitlab.com/calvin0x00/iot-xiu-dou.git
> cd iot-xiu-dou/
> npm install
```

## Usage
### 1. Run xiudou server, as a echo serive for IoT devices status 
```
> node xiudou_server.js
```

### 2. Run xiudou client app, 
```
> node xiudou_client.js [Device ID]
```
Device ID = 001~006, default is 001
### Example
#### Get device status by default
```
$ node xiudou_client.js
Binding status:
 { errorCode: 0,
  deviceId: '001',
  deviceType: 'outlet',
  isBound: false }
```

#### Get device 004 status
```
$ node xiudou_client.js 004
Binding status:
 { errorCode: 0,
  deviceId: '004',
  deviceType: 'lock',
  isBound: false }
```

#### Try to get nonexists device status
```
$ node xiudou_client.js 999
Binding status:
 { errorCode: 404,
  deviceId: '999',
  deviceType: '',
  isBound: false }

```


## Others
If you want to try change the status of above "devices", please refer to my LINE IoT chatbot demo: ["IoT-Binding-Bot"](https://docs.google.com/presentation/d/1BjkP5iU0KG8e_DfL0rHCUF8jlgEP8YML3NuynYGRtKw/edit?usp=sharing)