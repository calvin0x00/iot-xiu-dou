/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
const request = require('request');
const DEMO_3RDPARTY_API = 'https://iot-binding-bot.herokuapp.com/BindingStatus';

var PROTO_PATH = __dirname + '/protos/bindingstatus.proto';

var grpc = require('grpc');
var protoLoader = require('@grpc/proto-loader');
var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });
var bindingstatus_proto = grpc.loadPackageDefinition(packageDefinition).bindingstatus;

/**
 * Implements the SayHello RPC method.
 */
function echoBindingStatus(call, callback) {
  request
    .get({url: `${DEMO_3RDPARTY_API}/${call.request.deviceId}`,
        timeout: 30000}, function (err, res, body) {
      let deviceId = call.request.deviceId;
      let errorMessage = {
          deviceId: deviceId,
          deviceType: null,
          isBound: false
        };

      if(err){
        errorMessage['errorCode'] = 408;
        return callback(null, errorMessage);
      } else if(res.statusCode != 200){
        errorMessage['errorCode'] = res.statusCode;
        return callback(null, errorMessage);
      }

      let message = JSON.parse(body);
      message['errorCode'] = 0;
      return callback(null, message);
    });
}

/**
 * Starts an RPC server that receives requests for the BindingStatus service at the
 * sample server port
 */
function main() {
  var server = new grpc.Server();
  server.addService(bindingstatus_proto.BindingStatus.service, {echoBindingStatus: echoBindingStatus});
  server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure());
  server.start();
}

main();
